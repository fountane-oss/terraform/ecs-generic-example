[
  {
    "name": "app",
    "image": "${app_image}",
    "cpu": ${fargate_cpu},
    "memory": ${fargate_memory},
    "networkMode": "awsvpc",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "/ecs/app",
          "awslogs-region": "${aws_region}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "environment": [
        {
          "name": "DATABASE_HOST",
          "value": "${database_host}"
        },
        {
          "name": "DATABASE_PORT",
          "value": "${database_port}"
        },
        {
          "name": "DATABASE_NAME",
          "value": "${database_name}"
        },
        {
          "name": "DATABASE_USER",
          "value": "${database_user}"
        },
        {
          "name": "DATABASE_PASSWORD",
          "value": "${database_password}"
        }
    ],
    "portMappings": [
      {
        "containerPort": ${app_port},
        "hostPort": ${app_port}
      }
    ]
  }
]