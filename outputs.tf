
# outputs.tf

output "alb_hostname" {
  value = aws_alb.main.dns_name
}

output "database_hostname" {
  value = aws_db_instance.data.address
}

output "database_port" {
  value = aws_db_instance.data.port
}

output "database_name" {
  value = aws_db_instance.data.name
}

output "database_username" {
  value = aws_db_instance.data.username
}

output "database_password" {
  value = aws_db_instance.data.password
}
