resource "aws_db_subnet_group" "default" {
  name       = "main"
  subnet_ids = [aws_subnet.private[0].id, aws_subnet.private[1].id]

  tags = {
    Name = "DB subnet group"
  }
}


resource "aws_db_instance" "data" {
    allocated_storage = 20
    allow_major_version_upgrade = null
    apply_immediately = null
    auto_minor_version_upgrade = true
    backup_retention_period = 7
    backup_window = "03:42-04:12"
    copy_tags_to_snapshot = true
    db_subnet_group_name = aws_db_subnet_group.default.id # Put this in the private subnet
    delete_automated_backups = true
    deletion_protection = false
    enabled_cloudwatch_logs_exports = []
    engine = "postgres"
    engine_version = "12.5"
    iam_database_authentication_enabled = true
    identifier = "app-database"
    instance_class = "db.t3.small"
    license_model = "postgresql-license"
    maintenance_window = "fri:10:27-fri:10:57"
    max_allocated_storage = 1000
    monitoring_interval = 0
    multi_az = false
    option_group_name = "default:postgres-12"
    parameter_group_name = "default.postgres12"
    performance_insights_enabled = true
    performance_insights_retention_period = 7
    publicly_accessible = false
    replicate_source_db = ""
    security_group_names = []
    skip_final_snapshot = true
    snapshot_identifier = null
    storage_encrypted = true
    storage_type = "gp2"
    tags = {}
    timezone = ""
    username = var.database_username 
    password = var.database_password
    name = var.database_name # var.database_name
    vpc_security_group_ids = [
        aws_security_group.rds_database.id
    ]
}

